/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kisanogroup.test.thread;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;

/**
 *
 * @author ixm
 */

public abstract class KisanoThread extends Thread {
    private final Logger threadLogger = LogManager.getLogger(KisanoThread.class);
    private boolean busy = false;
    private boolean exiting = false;
    private int sleepPeriod = 1000;

    private DateTime lastExecutedOn = null;

    public KisanoThread() {
        super();
    }

    public KisanoThread(String threadName) {
        this(threadName, null);
    }

    public KisanoThread(String threadName, String baseThreadName) {
        super(threadName);

        threadLogger.info("Creating thread with name: "+ threadName);
    }

    public boolean isBusy() {
        return busy;
    }

    public void setBusy(boolean busy) {
        this.busy = busy;
    }

    public boolean isExiting() {
        return exiting;
    }

    public void setExiting(boolean exiting) {
        this.exiting = exiting;
    }

    public final void setSleepPeriod(int sleepPeriod) {
        this.sleepPeriod = sleepPeriod;
    }

    public int getSleepPeriod() {
        return sleepPeriod;
    }

    @Override
    public void run() {
        while (true) {
            setBusy(true);
            try {
                lastExecutedOn = DateTime.now();
                doWork();
            } catch (Throwable e) {
                threadLogger.error("Exception in doWork(): "+ e.getMessage(), e);
                threadLogger.error(e.getStackTrace());
                if(e.getCause() != null)
                    threadLogger.error(e.getCause().getMessage(), e.getCause());
            }
            setBusy(false);

            if (isExiting()) {
                threadLogger.debug("EXITING");
                break;
            }
            try {
                Thread.sleep(getSleepPeriod());
            } catch (InterruptedException e) {
                if (isExiting()) {
                    threadLogger.debug("EXITING IN INTERRUPT");
                    break;
                }
            }
        }
    }

    protected abstract void doWork();

    public DateTime getLastExecutedOn() {
        return lastExecutedOn;
    }

    public void setLastExecutedOn(DateTime lastExecutedOn) {
        this.lastExecutedOn = lastExecutedOn;
    }
}
